package tdd.training.bsk;

public class Game {
	private static final int FRAME_NUMBER = 10;
	private final Frame[] Frames = new Frame[FRAME_NUMBER];
	private int inizializedFrames = 0;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {

	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (inizializedFrames < FRAME_NUMBER) {
			Frames[inizializedFrames] = frame;
			inizializedFrames++;
		} else
			throw new BowlingException("Maximum frame number has been reached");
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index >= 0 && index < FRAME_NUMBER)
			if (index < inizializedFrames)
				return Frames[index];
			else
				throw new BowlingException("Frame at specified index has not been inizialized");
		else
			throw new BowlingException("Index out of bounds");
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if (firstBonusThrow >= 0 && firstBonusThrow <= 10)
			this.firstBonusThrow = firstBonusThrow;
		else
			throw new BowlingException("Invalid number of pins knocked");
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if (secondBonusThrow >= 0 && secondBonusThrow <= 10)
			this.secondBonusThrow = secondBonusThrow;
		else
			throw new BowlingException("Invalid number of pins knocked");
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {

		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int gameScore = 0;
		for (int i = 0; i < FRAME_NUMBER; i++) {
			if (i == FRAME_NUMBER - 1) {
				if (Frames[i].isSpare())
					Frames[i].setBonus(getFirstBonusThrow());
				if (Frames[i].isStrike())
					Frames[i].setBonus(getFirstBonusThrow() + getSecondBonusThrow());
				gameScore += Frames[i].getScore();
			} else {
				if (Frames[i].isSpare()) {
					Frames[i].setBonus(Frames[i + 1].getFirstThrow());
				}
				if (Frames[i].isStrike()) {
					if (Frames[i + 1].isStrike()) {
						if (i == FRAME_NUMBER - 2 && Frames[i + 1].isStrike())
							Frames[i].setBonus(10 + getFirstBonusThrow());
						else
							Frames[i].setBonus(10 + Frames[i + 2].getFirstThrow());
					} else
						Frames[i].setBonus(Frames[i + 1].getFirstThrow() + Frames[i + 1].getSecondThrow());
				}
				gameScore += Frames[i].getScore();
			}
		}
		return gameScore;
	}

}
