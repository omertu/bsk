package bsk;

import tdd.training.bsk.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testGameIsRight() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(3, g.getFrameAt(1).getFirstThrow());
		assertEquals(6, g.getFrameAt(1).getSecondThrow());

	}

	@Test
	public void testGameScoreIsCorrect() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(81, g.calculateScore());
	}

	@Test
	public void testGameScoreWithSpareIsCorrect() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 9));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(88, g.calculateScore());

	}

	@Test
	public void testGameScoreWithStrikeIsCorrect() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(94, g.calculateScore());

	}

	@Test
	public void testGameScoreWithSpareAfterStrike() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(4, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(103, g.calculateScore());

	}

	@Test
	public void testGameScoreWithMultipleStrike() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(112, g.calculateScore());

	}

	@Test
	public void testGameScoreWithMultipleSpares() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(8, 2));
		g.addFrame(new Frame(5, 5));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		assertEquals(98, g.calculateScore());

	}

	@Test
	public void testGameSpareAtLastFrame() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 8));
		g.setFirstBonusThrow(7);

		assertEquals(90, g.calculateScore());

	}

	@Test
	public void testGameStrikeAtLastFrame() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(10, 0));
		g.setFirstBonusThrow(7);
		g.setSecondBonusThrow(2);

		assertEquals(92, g.calculateScore());

	}

	@Test
	public void testPerfectGame() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.addFrame(new Frame(10, 0));
		g.setFirstBonusThrow(10);
		g.setSecondBonusThrow(10);

		assertEquals(300, g.calculateScore());

	}

	@Test(expected = BowlingException.class)
	public void testMoreThanTenFrameException() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));
		g.addFrame(new Frame(1, 1));
	}

	@Test(expected = BowlingException.class)
	public void testGetFrameAtIndexOutOfBoundsException() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));

		g.getFrameAt(10);
	}

	@Test(expected = BowlingException.class)
	public void testGetFrameAtNotInitializedFrameException() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));

		g.getFrameAt(4);
	}

	@Test(expected = BowlingException.class)
	public void testInvalidPinsKnockedAtFirstBonusThrow() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(8, 2));
		g.setFirstBonusThrow(-1);

	}
	
	
	@Test (expected = BowlingException.class)
	public void testInvalidPinsKnockedAtSecondBonusThrow() throws BowlingException {
		Game g = new Game();

		g.addFrame(new Frame(1, 5));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6));
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(10, 0));
		g.setFirstBonusThrow(7);
		g.setSecondBonusThrow(13);

	}
}