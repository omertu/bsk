package bsk;

import tdd.training.bsk.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class FrameTest {

	@Test
	public void testFistThrow() throws BowlingException {
		Frame f = new Frame(2, 4);
		assertEquals(2, f.getFirstThrow());
	}

	@Test
	public void testSecondThrow() throws BowlingException {
		Frame f = new Frame(2, 4);
		assertEquals(4, f.getSecondThrow());
	}

	@Test(expected = BowlingException.class)
	public void testFrameException() throws BowlingException {
		new Frame(5, 6);
	}

	@Test
	public void testScore() throws BowlingException {
		Frame f = new Frame(2, 4);
		assertEquals(6, f.getScore());
	}
}
